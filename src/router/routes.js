const routes = [
  {
    path: "/dashboard",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      {
        path: "",
        name: "dashboard",
        component: () => import("pages/Dashboard.vue")
      }
    ]
  },
  {
    path: "/vendas",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      {
        path: "oss",
        name: "salesServiceOrders",
        component: () => import("pages/sales/SalesServiceOrders.vue")
      },
      {
        path: "pedidos",
        name: "salesOrdereds",
        component: () => import("pages/sales/SalesOrdereds.vue")
      }
    ]
  },
  {
    path: "/cadastros",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      {
        path: "clentes",
        name: "regClients",
        component: () => import("pages/registrations/RegClients.vue")
      },
      {
        path: "fornecedores",
        name: "regProviders",
        component: () => import("pages/registrations/RegProviders.vue")
      },
      {
        path: "transportadoras",
        name: "regCarriers",
        component: () => import("pages/registrations/RegCarriers.vue")
      },
      {
        path: "produtos",
        name: "regProducts",
        component: () => import("pages/registrations/RegProducts.vue")
      }
    ]
  },
  {
    path: "/financas",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      {
        path: "relatorios",
        name: "finReports",
        component: () => import("pages/financial/FinReports.vue")
      }
    ]
  }
];

// Always leave this as last one
if (process.env.MODE !== "ssr") {
  routes.push({
    path: "*",
    redirect: "/dashboard"
  });
}

export default routes;
