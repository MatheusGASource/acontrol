module.exports = {
  root: true,

  parserOptions: {
    parser: "babel-eslint",
    sourceType: "module"
  },

  env: {
    browser: true
  },

  extends: [
    // Uncomment any of the lines below to choose desired strictness
    // See https://eslint.vuejs.org/rules/#available-rules
    "eslint:recommended",
    "plugin:prettier/recommended",
    "plugin:vue/recommended", // (Minimizing Arbitrary Choices and Cognitive Overhead)
    "prettier/vue"
  ],

  // required to lint *.vue files
  plugins: ["vue", "prettier"],

  globals: {
    ga: true, // Google Analytics
    cordova: true,
    __statics: true,
    process: true,
    Capacitor: true,
    chrome: true
  },

  // add your custom rules here
  rules: {
    "prefer-promise-reject-errors": "off",

    // allow debugger during development only
    "no-debugger": process.env.NODE_ENV === "production" ? "error" : "off"
  }
};
